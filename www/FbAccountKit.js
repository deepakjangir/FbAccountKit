var exec = require('cordova/exec');

exports.mobileLoginWithTheme = function login (s, f, t) {
    exec(s, f, 'FbAccountKit', 'mobileLogin', [t])
}

exports.emailLoginWithTheme = function login (s, f, t) {
    exec(s, f, 'FbAccountKit', 'emailLogin', [t])
}

exports.mobileLogin = function login (options,s, f) {
    exec(s, f, 'FbAccountKit', 'mobileLogin', [options])
}

exports.emailLogin = function login (options,s, f) {
    exec(s, f, 'FbAccountKit', 'emailLogin', [options])
}
