# FbAccountKit

Cordova plugin for SMS and Email verification using facebook accountkit.

## Command

cordova plugin add https://gitlab.com/deepakjangir/FbAccountKit.git --save --variable APP_ID="YourAppId" --variable  APP_NAME="YourAppName" --variable AK_TOKEN="XYZ123" --variable API_VERSION="v1.0"

## Installation

Make sure you've registered your Facebook app with Facebook and have an `APP_ID` on [https://developers.facebook.com/apps](https://developers.facebook.com/apps) and `AccountKitClientToken` on [https://developers.facebook.com/apps/<APP_ID>/account-kit/](https://developers.facebook.com/apps/<APP_ID>/account-kit/).

```bash
$ cordova plugin add https://gitlab.com/deepakjangir/FbAccountKit.git --save --variable APP_ID="YourAppId" --variable  APP_NAME="YourAppName" --variable AK_TOKEN="XYZ123" --variable API_VERSION="v1.0"
```

If you need to change your `APP_ID`, `APP_NAME`, or `AK_TOKEN` after installation, it's recommended that you remove and then re-add the plugin as above. Note that changes to the `APP_ID`, `APP_NAME`, or `AK_TOKEN` value in your `config.xml` file will *not* be propagated to the individual platform builds.

### Android

It is recommended to add these permission to `config.xml` of your Cordova project to reduce the friction during the login process ([more info](https://developers.facebook.com/docs/accountkit/android/configuration)):
```xml
<uses-permission android:name="android.permission.RECEIVE_SMS" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
```


## Usage

### Email Login

`FbAccountKit.emailLogin(options,function (response) { alert(JSON.stringify(response)); }, function (error) { console.log(error) });`

Success function will response like:

{
accessToken: "<long string>",
provider: "<string>",
id: "<string>",
email: "<email>"
}

Available options:
    {
        defaultCountryCode: "IN",
        facebookNotificationsEnabled: true,
        useAccessToken: true,
        setInitialEmail: "set_initial_email_here"
    }

### Mobile Login

`FbAccountKit.mobileLogin(options,function (response) { alert(JSON.stringify(response)); }, function (error) { console.log(error) });`

Success function will response like:

{
accessToken: "<long string>",
provider: "<string>",
id: "<string>",
mobile: "<mobile>"
}

Available options:
    {
        defaultCountryCode: "IN",
        facebookNotificationsEnabled: true,
        setReceiveSMS: true,
        useAccessToken: true,
        setFacebookNotificationsEnabled: true,
        setInitialPhoneNumber: ["Country Code", "Mobile Number"]
    }


## Colors customizations

### Android

`Plugin have defualt values now but you can change colors according to your requirements in /android/res/values/FbAccountKit.xml file or comment all '<item> tags to achieve default theme of account kit'`

`File :: res/values/FbAccountKit.xml`

`<style name="AccountKitLoginTheme" parent="Theme.AccountKit">`
`<item name="com_accountkit_background_color">#EEF6FF</item>`
`<item name="com_accountkit_button_background_color">#96BDEB</item>`
`<item name="com_accountkit_button_disabled_background_color">#96BDEB</item>`
`<item name="com_accountkit_button_border_color">#96BDEB</item>`
`<item name="com_accountkit_button_text_color">#ffffff</item>`
`<item name="com_accountkit_header_background_color">#96BDEB</item>`
`<item name="com_accountkit_header_text_color">#ffffff</item>`
`<item name="com_accountkit_input_background_color">#EEF6FF</item>`
`<item name="com_accountkit_input_border_color">#96BDEB</item>`
`<item name="com_accountkit_input_text_color">#da4a11</item>`
`<item name="com_accountkit_text_color">#da4a11</item>`
`<item name="com_accountkit_title_text_color">#da4a11</item>`
`</style>`

you will find above style in file res/values/FbAccountKit.xml. Change these colors accordingly.

